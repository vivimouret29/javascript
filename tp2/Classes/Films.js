import Media from "./Media.js";
import Helpers from "../Helpers.js";

export default class Film extends Media {
    constructor(data) {
        super(data)
    }

    remplir() {
        super.remplir()
        Helpers.remplirChamp("nom", this.data.title)
        document.title = `Film : ${this.data.title}`
        this.remplirAnne(this.data.release_date)
    }
}