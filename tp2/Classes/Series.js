import Media from "./Media.js";
import Helpers from "../Helpers.js";

export default class Serie extends Media {
    constructor(data) {
        super(data)
        this.cacherPanneau("film")
    }

    remplir() {
        super.remplir()
        // console.log(this.data)
        Helpers.remplirChamp("nom", this.data.name)
        document.title = `Série : ${this.data.name}`
        this.remplirAnne(this.data.first_air_date)

        let codeHtml = ""
        this.data.seasons.forEach(element => {
            codeHtml += `<li>${item.name} : ${item.episode.count} épisodes</li>`
        })
        Helpers.id(detailsSaisons).innerHTML = codeHtml
        Helpers.remplirChamp("nbSaisons", this.data.number_of_seasons)
    }
}