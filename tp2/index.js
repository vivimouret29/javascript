import Helpers from "./Helpers.js";
import Media from "./Classes/Media.js";
import Film from "./Classes/Films.js";
import Serie from "./Classes/Series.js";

const apiKey = "f517d56925f4a842bdbbf82b24f961d0"

const traiterMedia = (data, type) => {
    const media = type == "movie" ? new Film(data) : new Serie(data)
    media.remplir()
}

const chargerMedia = () => {
    const id = Helpers.getParam("id")
    const type = Helpers.getParam("type")
    const url = `https://api.themoviedb.org/3/${type}/${id}?api_key=${apiKey}&language=fr-FR`
    axios
        .get(url)
        .then(response => traiterMedia(response.data, type))
        .catch(error => {
            if (error.response && error.response.status == 404) {
                alert("Média introuvable")
            } else {
                console.error(error)
            }
        })
}

window.addEventListener("load", chargerMedia)

// hhttp://127.0.0.1:5500/tp2/index.html?id=1402&type=tv
// http://127.0.0.1:5500/tp2/index.html?id=603&type=movie