const axios = require("axios");

const tryResult = resultat => {
    const { title, artist, album, duration } = resultat;
    const timeMin = Math.floor(duration / 60);
    const timSec = (duration % 60).toString().padStart(2, "0");
    console.log(
        `Titre : ${title}, Artiste : ${artist.name}, Album : ${album.title}, Durée : ${timeMin}mn ${timSec}s`
    );
};

axios 
    .get(`https://api.deezer.com/search?q=${process.argv[2]}`)
    .then(result => {
        const { data: resultrack } = result.data;
        let nbtrack = 0;
        for (let i = 0; i < resultrack.length; i++) {
            const element = resultrack[i];
            if (element.type !== "track") continue;
            tryResult(element);
            nbtrack++;
            if (nbtrack >= 10) break;        
        };
        if (nbtrack === 0) {
            console.log("Pas de résultats !")
        };
    })
    .catch(error => console.error(error))
